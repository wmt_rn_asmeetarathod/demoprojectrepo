package com.example.demoproject;

import android.content.Context;

public class LoginPresenter implements  LoginView{


    LoginView view;

    public LoginPresenter(LoginView view) {
        this.view = view;
    }

    @Override
    public String checkUser(String userName, String Pswd, Context context) {
        String response="0";
        DBHelper helper=new DBHelper(context);
        response=helper.checkUserData(userName,Pswd);
        return response;
    }

}
