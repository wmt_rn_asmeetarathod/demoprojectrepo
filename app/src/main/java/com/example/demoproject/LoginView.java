package com.example.demoproject;

import android.content.Context;

public interface LoginView {

    String checkUser(String s1, String s2, Context context);

}
