package com.example.demoproject;

import android.content.Context;

public interface ProfileView {

    public DataClassSignUp getUserData(String userId, Context context);

    public  int updateData(DataClassSignUp dc);

}
