package com.example.demoproject;

import android.os.Bundle;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpFragment extends Fragment implements SignUpView, View.OnClickListener {

    EditText edFirstName, edLastName, edContact, edEmail, edPassword, edConfirmPassword;
    String firstname, lastname, contact, email, password, confirmpassword;
    Button btSignUp;
    SignUpPresenter presenter;
    AwesomeValidation awesomeValidation;

    public SignUpFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // EditText....
        edFirstName = view.findViewById(R.id.edtFirstName);
        edLastName = view.findViewById(R.id.edtLastName);
        edContact = view.findViewById(R.id.edtContact);
        edEmail = view.findViewById(R.id.edtEmail);
        edPassword = view.findViewById(R.id.edtSPassword);
        edConfirmPassword = view.findViewById(R.id.edtSConfirmPassword);

        btSignUp = view.findViewById(R.id.btnSignUp);


        presenter = new SignUpPresenter(this);

        btSignUp.setOnClickListener(this);

    }


    @Override
    public void putData(DataClassSignUp dc) {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onClick(View v) {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(getActivity(), R.id.edtFirstName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
        awesomeValidation.addValidation(getActivity(), R.id.edtLastName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
        awesomeValidation.addValidation(getActivity(), R.id.edtContact, "^[0-9]{2}[0-9]{8}$", R.string.contactError);
        awesomeValidation.addValidation(getActivity(), R.id.edtEmail, Patterns.EMAIL_ADDRESS, R.string.emailError);
        awesomeValidation.addValidation(getActivity(), R.id.edtSPassword, "[0-9]{5}$", R.string.pswdError);
        awesomeValidation.addValidation(getActivity(), R.id.edtSConfirmPassword, "[0-9]{5}$", R.string.confirmPswdError);


        // Get Values....
        firstname = edFirstName.getText().toString();
        lastname = edLastName.getText().toString();
        contact = edContact.getText().toString();
        email = edEmail.getText().toString();
        password = edPassword.getText().toString();
        confirmpassword = edConfirmPassword.getText().toString();

        if (awesomeValidation.validate()) {


            if (password.equals(confirmpassword)) {

                //insert Data into Database

                presenter.putData(new DataClassSignUp(firstname, lastname, contact, email, password, v.getContext()));
                Toast.makeText(v.getContext(), "SignUp Successfully", Toast.LENGTH_LONG).show();


                // Replace SignUp Fragment to Login Fragment

                LoginFragment loginFragment = new LoginFragment();
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment1, loginFragment);

                fragmentTransaction.hide(this);
                fragmentTransaction.hide(new ProfileFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(v.getContext(), "Both Password are NOT Same! Try Again !", Toast.LENGTH_LONG).show();
            }
        }

    }
}