package com.example.demoproject;

import android.content.Context;
import android.os.Bundle;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class ProfileFragment extends Fragment implements ProfileView, View.OnClickListener {


    AwesomeValidation awesomeValidation;
    Button btLogout, btUpdateProfile;
    String userId;
    String fName, lName, contact, emailId;
    ProfilePresenter profilePresenter;
    EditText tvfName, tvlName, tvEmail, tvContact;

    public ProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        assert getArguments() != null;
        userId = getArguments().getString("userLoginId");
        Log.e("userId in Profile", userId);
        profilePresenter = new ProfilePresenter(this);
        DataClassSignUp dc = profilePresenter.getUserData(userId, getContext());


        tvfName = view.findViewById(R.id.tvfName);
        tvlName = view.findViewById(R.id.tvlName);
        tvContact = view.findViewById(R.id.tvContact);
        tvEmail = view.findViewById(R.id.tvEmail);


        tvfName.setText(dc.getFirstName());
        tvlName.setText(dc.getLastName());
        tvContact.setText(dc.getContactNo());
        tvEmail.setText(dc.getsEmail());

        btLogout = view.findViewById(R.id.btnLogOut);
        btLogout.setOnClickListener(this);

        btUpdateProfile = view.findViewById(R.id.btnUpdateProfile);
        btUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);


                awesomeValidation.addValidation(getActivity(), R.id.tvfName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
                awesomeValidation.addValidation(getActivity(), R.id.tvlName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameError);
                awesomeValidation.addValidation(getActivity(), R.id.tvContact, "^[0-9]{2}[0-9]{8}$", R.string.contactError);
                awesomeValidation.addValidation(getActivity(), R.id.tvEmail, Patterns.EMAIL_ADDRESS, R.string.emailError);

                if (awesomeValidation.validate()) {


                    fName = tvfName.getText().toString();
                    lName = tvlName.getText().toString();
                    contact = tvContact.getText().toString();
                    emailId = tvEmail.getText().toString();
                    int res = 0;
                    res = profilePresenter.updateData(new DataClassSignUp(v.getContext(), fName, lName, contact, emailId, userId));
                    if (res == 1) {
                        Toast.makeText(getContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "Try Again!", Toast.LENGTH_LONG).show();
                    }
                }


            }
        });

        return view;
    }


    @Override
    public DataClassSignUp getUserData(String userId, Context context) {

        return null;
    }

    @Override
    public int updateData(DataClassSignUp dc) {
        return 0;
    }

    @Override
    public void onClick(View v) {
        callLoginFragment();
        SessionManager session = new SessionManager(v.getContext());
        session.logOutUser();

    }

    public void callLoginFragment() {
        LoginFragment loginFragment = new LoginFragment();

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment1, loginFragment);
        fragmentTransaction.hide(this);
        fragmentTransaction.hide(new SignUpFragment());
        fragmentTransaction.commit();
    }
}