package com.example.demoproject;

import android.content.Context;

public class ProfilePresenter implements ProfileView {

    ProfileView profileView;

    public ProfilePresenter(ProfileView profileView) {
        this.profileView = profileView;
    }

    @Override
    public DataClassSignUp getUserData(String userId, Context context) {
        DBHelper helper=new DBHelper(context);
        DataClassSignUp dc=helper.getUserDataById(userId);
        return dc;
    }

    @Override
    public int updateData(DataClassSignUp dc) {
        int res=0;
        DBHelper helper=new DBHelper(dc.getContext());
        res=helper.updateUserProfile(dc);
        return res;


    }


}
