package com.example.demoproject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;


public class DBHelper extends SQLiteOpenHelper {

    public SharedPreferences sp;

    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    public DBHelper(@Nullable Context context) {
        super(context, "dbUsers", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS tbl_users(id INTEGER PRIMARY KEY,firstname VARCHAR,lastname VARCHAR,contact VARCHAR,userEmail VARCHAR,password VARCHAR );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tbl_users");
        onCreate(db);
    }

    public void addUser(DataClassSignUp dataClassSignUp) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("firstname", dataClassSignUp.getFirstName());
        values.put("lastname", dataClassSignUp.getLastName());
        values.put("contact", dataClassSignUp.getContactNo());
        values.put("userEmail", dataClassSignUp.getsEmail());
        values.put("password", dataClassSignUp.getsPassword());
        db.insert("tbl_users", null, values);
        Log.e("SignUp Data", "Data Inserted");
        db.close();

    }

    public String checkUserData(String uName, String uPassword) {
        SQLiteDatabase db = this.getWritableDatabase();
        //Cursor cursor = db.query("tbl_users", null, "userEmail = ?", new String[]{uName}, null, null, null, null);

        String sql="select * from tbl_users where userEmail='"+uName+"'";
        Cursor cursor1=db.rawQuery(sql,null);

        if (cursor1 != null & cursor1.moveToFirst()) {
            String res=cursor1.getString(cursor1.getColumnIndex("id"));
            Log.e("res",res);
            cursor1.close();
            return res;
            //return 1;
        } else {
            return "0";
        }


    }

    public DataClassSignUp getUserDataById(String userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        DataClassSignUp dc;
        Cursor cursor = db.query("tbl_users", null, "id=?", new String[]{userId}, null, null, null, null);
        if (cursor != null & cursor.moveToPosition(0)) {
            dc = new DataClassSignUp(cursor.getString(cursor.getColumnIndex("firstname")),
                    cursor.getString(cursor.getColumnIndex("lastname")),
                    cursor.getString(cursor.getColumnIndex("contact")),
                    cursor.getString(cursor.getColumnIndex("userEmail")));
        } else {
            dc = null;
        }

        return dc;
    }

    public int updateUserProfile(DataClassSignUp dc){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("firstname",dc.getFirstName());
        values.put("lastname",dc.getLastName());
        values.put("contact",dc.getContactNo());
        values.put("userEmail",dc.getsEmail());
        return db.update("tbl_users",values,"id=?",new String[]{dc.getUserId()});

    }
}
















