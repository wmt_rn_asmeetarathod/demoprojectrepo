package com.example.demoproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class LoginFragment extends Fragment implements View.OnClickListener, LoginView {

    SharedPreferences sp;
    EditText edUserName, edlPassword;
    String sUserName, sPassword;
    TextView tvSignUp;
    Button btLogin;
    LoginPresenter presenter;
    String res;
    AwesomeValidation awesomeValidation;

    public LoginFragment() {

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edUserName = view.findViewById(R.id.edtUserName);
        edlPassword = view.findViewById(R.id.edtLPassword);

        btLogin = view.findViewById(R.id.btnLogin);

        presenter = new LoginPresenter(this);
        btLogin.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        tvSignUp = view.findViewById(R.id.tvNotSignUp);
        tvSignUp.setOnClickListener(v -> {
            SignUpFragment signUpFragment = new SignUpFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment1, signUpFragment);

            fragmentTransaction.hide(this);
            fragmentTransaction.hide(new ProfileFragment());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        });
        return view;
    }

    @Override
    public void onClick(View v) {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(getActivity(), R.id.edtUserName, Patterns.EMAIL_ADDRESS, R.string.emailError);
        awesomeValidation.addValidation(getActivity(), R.id.edtLPassword, "[0-9]{5}$", R.string.pswdError);

        if (awesomeValidation.validate()) {


            SessionManager session = new SessionManager(v.getContext());
            sUserName = edUserName.getText().toString();
            sPassword = edlPassword.getText().toString();
            res = presenter.checkUser(sUserName, sPassword, v.getContext());
            if (res.equals("0")) {

                Toast.makeText(getContext(), "Invalid Username or Password!", Toast.LENGTH_LONG).show();

            } else {
                session.createLoginSession(res, sUserName);
                callProfileFragment();
                Toast.makeText(getContext(), "Login Successfully", Toast.LENGTH_LONG).show();

            }
        }

    }

    public void callProfileFragment() {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString("userLoginId", res);
        profileFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment1, profileFragment);
        fragmentTransaction.hide(this);
        fragmentTransaction.hide(new SignUpFragment());
        fragmentTransaction.commit();
    }


    @Override
    public String checkUser(String s1, String s2, Context context) {
        return "0";
    }


}