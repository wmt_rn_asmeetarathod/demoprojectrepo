package com.example.demoproject;

public interface SignUpView {

    void putData(DataClassSignUp dc);

    void showLoader();

    void hideLoader();
}
