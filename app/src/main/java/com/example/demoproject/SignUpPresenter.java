package com.example.demoproject;

import android.util.Log;

public class SignUpPresenter implements SignUpView{


    SignUpView view;
    public SignUpPresenter(SignUpView view) {
        this.view=view;
    }

    @Override
    public void putData(DataClassSignUp dc) {
        DBHelper dbHelper = new DBHelper(dc.getContext());
        dbHelper.addUser(dc);
        Log.e("Inserted","SignUp Data Inserted");

    }



    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
}
